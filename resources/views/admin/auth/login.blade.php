@extends('layouts.app')
@section('title','Connexion')
@section('css','connexion.css')
@section('body','admin')
@section('content')
<div style="height:20%"></div>
<div class="ui raised very padded text container segment">
  <h3>Accès au support</h3>
  <div class="ui container">
    <div class="row">
      <h5>Connectez-vous monsieur l'agent</h5>
      <form class="" role="form" method="POST" action="{{ url('/admin/login') }}">
          {{ csrf_field() }}

          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="ui labeled input" id="field">
              <div class="ui red label" style="width:19.8%; text-align:center">
                email
              </div>
              <input id="email" type="email" placeholder="email" name="email" required autofocus>
            </div>
          </div>
          <br>
          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="ui labeled input" id="field">
              <div class="ui red label">
                mot de passe
              </div>
              <input id="password" type="password" placeholder="mot de passe" name="password" required>
            </div>
          </div>
          <br>
          @if ($errors->has('email'))
              <span class="help-block" style="color:red">
                  <strong>L'email et le mot de passe ne se correspondent pas.</strong>
              </span><br>
          @endif
          @if ($errors->has('password'))
              <span class="help-block">
                  <strong>L'email et le mot de passe ne se correspondent pas.</strong>
              </span><br>
          @endif
          <div class="ui toggle checkbox">
            <input type="checkbox" name="remember">
            <label>Se souvenir de moi</label>
          </div>
          <br>
          <br>
          <button type="submit" class="ui right labeled icon red button" id="submit">
            <i class="right arrow icon"></i>
            se connecter
          </button>
            <a class="btn btn-link" href="{{ url('/password/reset') }}">
                mot de passe oublié ?
            </a>
          </div>
      </form>
    </div>
  </div>
</div>
@endsection
