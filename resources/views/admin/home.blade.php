@extends('layouts.app')
@section('title','Connexion')
@section('css','connexion.css')
@section('body','admin')
@section('content')
<div style="height:20%"></div>
<div class="ui raised very padded text container segment">
    <h3>Connexion réussi</h3>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function () {
      window.location.replace("http://dev.optimis.fr/support");
    }, 200);
  });
</script>
@endsection
