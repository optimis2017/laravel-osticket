<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/semantic-ui/2.2.8/semantic.min.css">
    <link rel="stylesheet" href="{{asset('css/')}}/@yield('css')">
    <title>@yield('title')</title>
  </head>
  <body id="@yield('body')">
    @yield('content')
  <footer>
    <p>Copyright © 2017 Optimis - All rights reserved.</p>
  </footer>
  </body>
</html>
