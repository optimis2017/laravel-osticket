<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="/images/favicons/@yield('icon')" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/semantic-ui/2.2.8/semantic.min.css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/')}}/@yield('css')">
    <link rel="stylesheet" href="{{asset('css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('css/slick-theme.css')}}">
    <title>@yield('title')</title>
  </head>
  <body id="body">
    @yield('content')
    <div id="kb-content" class="ui segment">
      <h1>Foire aux questions</h1>
      <h3>Catégories de FAQ :</h3>
      <ul id="faq">
        @php
          $liCount = 0;
          $liId = array();
          $faqId = array();
          $faqCount = array();
          $catId = array();
          $faqName = array();
        @endphp
      @foreach ($category as $key => $value)
        @if ($category[$key]['ispublic'] == 1)
          @php
            $count = 0;
          @endphp
          @foreach ($faq as $key2 => $value2)
            @if ($faq[$key2]['ispublished'] == 1)
              @if ($faq[$key2]['category_id'] == $category[$key]['category_id'])
                @php
                  $faqId[] = $key2;
                  $count += 1;
                @endphp
              @endif
            @endif
          @endforeach
          @if ($count != 0)
            @php
              $liCount += 1;
              $faqName[] = $category[$key]['name'];
              $faqCount[] = $count;
              $liId[] = $category[$key]['category_id'];
              $catId[] = "cat".$category[$key]['category_id'];
            @endphp
            <li><a id="cat{{$category[$key]['category_id']}}" href="#">{{$category[$key]['name']}} ({{$count}})</a></li>
          @endif
        @endif
      @endforeach
      </ul>
      @php
        $temp = 0;
        $questId = array();
      @endphp
      @for ($l=0; $l < $liCount ; $l++)
        <div id="sous-faq{{$l+1}}">
          <h3>{{$faqName[$l]}}</h3>
          <ul>
          @for ($j=0; $j < $faqCount[$l]; $j++)
            <li><a id="quest{{$faq[$faqId[$temp]]['faq_id']}}" data-title="{{$faq[$faqId[$temp]]['question']}}" data-updated="{{$faq[$faqId[$temp]]['updated']}}" data-id="{{$faq[$faqId[$temp]]['answer']}}" href="#"><span>{{$faq[$faqId[$temp]]['question']}}</span></a></li>
            @php
              $questId[$temp] = $faqId[$temp];
              $temp += 1;
            @endphp
          @endfor
          </ul>
        </div>
      @endfor
      <div id="question"></div>
      <a id="previous" href="#"><i class="chevron left big icon"></i></a>
    </div>
    <div id="new-content" class="ui segment">
      @if(Auth::check())
        <span>{{Auth::user()->name}}</span>
      @else
        <h3>Accès au support</h3>
            <h5>Connectez-vous</h5>
            <br><br>
            <form class="" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <div class="ui labeled input" id="field">
                    <div class="ui blue label">
                      &nbsp;&nbsp;&ensp;&ensp;&ensp;email&ensp;&ensp;&ensp;&nbsp;&nbsp;
                    </div>
                    <input type="email" placeholder="email" name="email" required autofocus>
                  </div>
                </div>
                <br>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <div class="ui labeled input" id="field">
                    <div class="ui blue label">
                      &nbsp;mot de passe
                    </div>
                    <input type="password" placeholder="mot de passe" name="password" required>
                  </div>
                </div>
                <br>
                @if ($errors->has('email'))
                    <span class="help-block" style="color:red">
                        <strong>L'email et le mot de passe ne se correspondent pas.</strong>
                    </span><br>
                @endif
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>L'email et le mot de passe ne se correspondent pas.</strong>
                    </span><br>
                @endif
                <div id="checkbox" class="ui toggle checkbox">
                  <input type="checkbox" name="remember">
                  <label>Se souvenir de moi</label>
                </div>
                <br>
                <br>
                <button type="submit" class="ui blue button" id="submit">
                  se connecter
                </button>  &emsp;&emsp;
                  <a id="link" href="{{ url('/password/reset') }}">
                    mot de passe oublié ?
                  </a>
            </form>
            <hr>
            <span>Êtes-vous un agent ? <a href="/admin">cliquer içi</a></span><br>
            <span>s'inscrire <a href="/admin">cliquer içi</a></span>
      @endif
    </div>
      <div id="status-content" class="ui segment">
        <p>dev4</p>
      </div>
      <div id="tickets-content" class="ui segment">
        <h3>Liste de tickets</h3>
        @php
          $userId = Auth::user()->id;
          $ticketCounter = 0;
          $even = false;
        @endphp
        <table id="tabTicket">
          <thead>
            <tr>
              <th><span>Ticket #</span></th>
              <th><span>Sujet</span></th>
              <th><span>Statut</span></th>
              <th><span>Date de création</span></th>
              <th><span>Service</span></th>
              <th><span>TMA</span></th>
            </tr>
          </thead>
          <tbody>
          @foreach ($ticket as $key => $value)
            @if ($userId == $ticket[$key]['user_id'])
              <tr>
              @php
                $ticketCounter += 1;
                $ticket_id = $ticket[$key]['ticket_id'];
              @endphp
              <td><a href="#"><span>{{$ticket[$key]['number']}}</span></a></td>
              @foreach ($data as $key => $value)
                @if ($ticket_id == $data[$key]['ticket_id'])
                  @php
                    $subject =  $data[$key]['subject'];
                  @endphp
                @endif
              @endforeach
              <td><a href="#"><span>{{$subject}}</span></a></td>
              @if ($ticket[$key]['status_id'] == 3)
                  <td><span>Fermé</span></td>
                @else
                  <td><span>Ouvert</span></td>
              @endif
              @php
                $date = $ticket[$key]['created'];
                $res =date_format($date,"d/m/Y");
              @endphp
              <td><span>{{$res}}</span></td>
              @php
                $dept ='';
                switch ($ticket[$key]['dept_id']) {
                  case '1':
                    $dept = 'Support';
                    break;
                  case '2':
                    $dept = 'Sales';
                    break;
                  case '3':
                    $dept = 'Maintenance';
                    break;
                  case '4':
                    $dept = 'Consultants';
                    break;
                  case '5':
                    $dept = 'AVEA';
                    break;
                  case '6':
                    $dept = 'TEST';
                    break;
                }
              @endphp
              <td><span>{{$dept}}</span></td>
              @php
                $tma = '';
              @endphp
              @foreach ($stock as $key => $value)
                @if ($ticket_id == $stock[$key]['ticket_id'])
                  @php
                    $tma = $stock[$key]['quantite'];
                  @endphp
                @endif
              @endforeach
              <td><span>{{$tma}}</span></td>
            </tr>
            @endif
          @endforeach
          </tbody>
        </table>
      </div>
    <div id="empty" class="ui segment"></div>
  </div>
  <footer>
    <p>Copyright © 2017 Optimis - All rights reserved.</p>
  </footer>
    <script type="text/javascript" src="{{asset('js/slick.js')}}"></script>
    <script src="{{asset('js/slick.min.js')}}" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.single-item').slick({
          dots: true,
          infinite: true,
          speed: 500,
          fade: true,
          cssEase: 'linear',
          autoplay: true,
          prevArrow: false,
          nextArrow: false
        });
        $('#home-content').show();
        $('#kb-content').hide();
        $('#new-content').hide();
        $('#status-content').hide();
        $('#tickets-content').hide();
        $('#empty').hide();
        $(".menu").click(function(){
          if ($("#home").hasClass("active")) {
            $('#home-content').show();
            $('#kb-content').hide();
            $('#new-content').hide();
            $('#status-content').hide();
            $('#tickets-content').hide();
          }
          if ($("#kb").hasClass("active")) {
            $('#kb-content').show();
            $('#home-content').hide();
            $('#new-content').hide();
            $('#status-content').hide();
            $('#tickets-content').hide();
          }
          if ($("#new").hasClass("active")) {
            $('#new-content').show();
            $('#home-content').hide();
            $('#kb-content').hide();
            $('#status-content').hide();
            $('#tickets-content').hide();
          }
          if ($("#status").hasClass("active")) {
            $('#status-content').show();
            $('#home-content').hide();
            $('#kb-content').hide();
            $('#new-content').hide();
            $('#tickets-content').hide();
          }
          if ($("#tickets").hasClass("active")) {
            $('#tickets-content').show();
            $('#status-content').hide();
            $('#home-content').hide();
            $('#kb-content').hide();
            $('#new-content').hide();
          }
        });
        $("#home").click(function(){
          $("#home").addClass("active");
          $("#kb").removeClass("active");
          $("#new").removeClass("active");
          $("#status").removeClass("active");
          $("#tickets").removeClass("active");
        });
        $("#kb").click(function(){
          $("#home").removeClass("active");
          $("#kb").addClass("active");
          $("#new").removeClass("active");
          $("#status").removeClass("active");
          $("#tickets").removeClass("active");
        });
        $("#new").click(function(){
          $("#home").removeClass("active");
          $("#kb").removeClass("active");
          $("#new").addClass("active");
          $("#status").removeClass("active");
          $("#tickets").removeClass("active");
        });
        $("#status").click(function(){
          $("#home").removeClass("active");
          $("#kb").removeClass("active");
          $("#new").removeClass("active");
          $("#status").addClass("active");
          $("#tickets").removeClass("active");
        });
        $("#tickets").click(function(){
          $("#home").removeClass("active");
          $("#kb").removeClass("active");
          $("#new").removeClass("active");
          $("#status").removeClass("active");
          $("#tickets").addClass("active");
        });
        for (var i = 1; i <= {{$liCount}}; i++) {
          $('#sous-faq'+i).hide();
        }
        $('#previous').hide();
        <?php
        $js_array = json_encode($catId);
        echo "var category = ". $js_array . ";\n";
        $js_array2 = json_encode($questId);
        echo "var question = ". $js_array2 . ";\n";
        ?>
        faq = true;
        sous_faq = false;
        question = false;
        currentFaq = 0;
        function insertFaq(id){
          var cat;
          cat = document.getElementById(category[id-1]);
          if (typeof window.addEventListener === 'function'){
              (function (_cat) {
                  cat.addEventListener('click', function(){
                    console.log(_cat);
                    $('#faq').hide();
                    faq = false;
                    $('#sous-faq'+id).show();
                    $('#sous-faq'+id).click(function(){
                      for (var t = 1; t <= {{$liCount}}; t++){
                          removeFaq(t);
                      }
                      currentFaq = id;
                      sous_faq = false;
                      var title = $('#sous-faq'+id+" ul li a").attr("data-title");
                      var h4 = $("<h4>"+title+"</h4>");
                      var content = $('#sous-faq'+id+" ul li a").attr("data-id");
                      $('#question').html(h4);
                      $('#question').append("<div style='height:20px'></div>");
                      $('#question').append("<hr>");
                      $('#question').append(content);
                      $('#question').append("<hr>");
                      var updated = $('#sous-faq'+id+" ul li a").attr("data-updated");
                      var res = updated.split(" ");
                      console.log(res);
                      var res2 = res[0].split("-");
                      console.log(res2);
                      var month = "";
                      switch (res2[1]) {
                        case "01": month = "Janvier";
                          break;
                        case "02": month = "Février";
                          break;
                        case "03": month = "Mars";
                          break;
                        case "04": month = "Avril";
                          break;
                        case "05": month = "Mai";
                          break;
                        case "06": month = "Juin";
                          break;
                        case "07": month = "Juillet";
                          break;
                        case "08": month = "Août";
                          break;
                        case "09": month = "Septembre";
                          break;
                        case "10": month = "Octobre";
                          break;
                        case "11": month = "Novembre";
                          break;
                        case "12": month = "Décembre";
                          break;
                        default: //do nothing
                      }
                      var date = res2[2]+" "+month+" "+res2[0];
                      console.log(date);
                      $('#question').append("Dernière mise à jour : "+date);
                      $("#question").show();
                      question = true;
                    });
                    sous_faq = true;
                    $('#previous').show();
                  });
              })(cat);
          }
        }
        function removeFaq(id){
          $('#sous-faq'+id).hide();
          sous_faq = false;
          faq = true;
        }
        for (var t = 1; t <= {{$liCount}}; t++){
            insertFaq(t);
        }
        $('#previous').click(function(){
          if (sous_faq == true) {
            for (var t = 1; t <= {{$liCount}}; t++){
                removeFaq(t);
            }
            $('#faq').show();
            $('#previous').hide();
          }
          if(question == true){
             $('#question').hide();
             $('#sous-faq'+currentFaq).show();
             question = false;
             sous_faq = true;
          }
        });
        localStorage.setItem('url',window.location.href);
      });
    </script>
  </body>
</html>
