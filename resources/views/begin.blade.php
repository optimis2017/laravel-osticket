<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/semantic-ui/2.2.8/semantic.min.css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <title>Choose a company</title>
  </head>
  <body>
    <div class="ui container">
      <hr>
      <h1>Choose a button to access to the website</h1>
      <hr>
      <div class="ui segments">
        <a href="/index" style="text-decoration:none; color:inherit"><button class="ui blue basic button">Optimis</button></a>
        <a href="/index-avea" style="text-decoration:none; color:inherit"><button class="ui orange basic button">Avea</button></a>
      </div>
    </div>
  </body>
</html>
