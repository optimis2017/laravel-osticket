@extends('templates.base')
@section('icon','favicon_Avea.png')
@section('title','Avea')
@section('css','avea.css')
@section('content')
  <div id="segment-container" class="ui segment container">
    <div class="ui two column doubling stackable grid container" id="header">
      <div class="left floated ten wide column">
        <img class="ui medium image" src="{{asset('images/logos/logoavea.png')}}" alt="Avea">
      </div>
      <div class="right floated six wide column">
        @if(Auth::check())
          <div class="ui vertically divided grid">
              <div class="column row" id="right-menu">
                <span>Bonjour {{Auth::user()->name}}</span>&emsp;&#124;&emsp;
                <a class="item" href="/profil"><span>Profil</span></a>	&emsp;&#124;&emsp;
                <a class="item" href="/logout"><i class="sign out large icon"></i><span>se déconnecter</span></a>
              </div>
              <div class="column row" id="right-menu">
                <p>Solde T.M.A ( )</p>&emsp;&#124;&emsp;
                <p>Version PMI ( )</p>
              </div>
            </div>
          @else
            <a href="/login"><div class="ui right floated orange button">connexion</div></a>
        @endif
      </div>
    </div>
    <div class="ui pointing menu">
      <a id="home" class="item active">
        <i class="big home orange icon"></i>
        <span>Accueil</span>
      </a>
      <a id="kb" class="item">
        <i class="large database blue icon"></i>
        <span>Bases de connaissances</span>
      </a>
      <a id="new" class="item">
        <i class="big plus square outline green icon"></i>
        <span>Nouveau ticket</span>
      </a>
      @if(Auth::check())
        <a id="tickets" class="item">
          <i class="big clone yellow icon"></i>
          <span>Tickets</span>
        </a>
        @else
        <a id="status" class="item">
          <i class="big checkmark box pink icon"></i>
          <span>Vérifier le statut d'un ticket</span>
        </a>
      @endif
    </div>
    <div id="home-content" class="ui segment">
      <h1>Bienvenue sur le support d'Avea</h1>
      <hr>
      <div class="ui two column doubling stackable grid container">
        <div class="five wide column">
          <div class="ui left floated green cleared button massive">Nouveau ticket</div>
        </div>
        <div class="eleven wide column">
          <div class="single-item">
              <div><img src="{{asset('images/caroussel/avea.png')}}" style="width:100%; height:auto"></div>
              <div><img src="{{asset('images/caroussel/buzi.png')}}" style="width:100%; height:auto"></div>
              <div><img src="{{asset('images/caroussel/chaine_valeur_erp.png')}}" style="width:100%; height:auto"></div>
          </div>
        </div>
      </div>
    </div>
@endsection
