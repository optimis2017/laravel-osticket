@extends('layouts.app')
@section('title','Connexion')
@section('css','connexion.css')
@section('body','login')
@section('content')
<div style="height:20%"></div>
<div class="ui raised very padded text container segment">
    <h3>Connexion réussi</h3>
</div>
@if (Auth::check())
  @php
    $orgId = Auth::user()->org_id;
    $teamId = "0";
      foreach ($org as $key => $value) {
        if ($orgId == $org[$key]["id"]) {
          if ($org[$key]["manager"] != null) {
            $array = str_split($org[$key]["manager"]);
            $teamId = $array[1];
           }
          }
        }
  @endphp
@endif
<script type="text/javascript">
  $(document).ready(function(){
    var teamId = <?php echo $teamId; ?>;
    if (teamId != 2) {
      setTimeout(function () {
        window.location.replace("http://dev.optimis.fr/index");
      }, 200);
    }else {
      setTimeout(function () {
        window.location.replace("http://dev.optimis.fr/index-avea");
      }, 200);
    }
  });
</script>
@endsection
