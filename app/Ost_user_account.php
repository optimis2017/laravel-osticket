<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_user_account extends Model
{
  protected $table = 'ost_user_account';
  const CREATED_AT = 'registered';
}
