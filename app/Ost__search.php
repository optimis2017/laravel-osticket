<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost__search extends Model
{
  protected $table = 'ost_search';
  public $timestamps = false;
}
