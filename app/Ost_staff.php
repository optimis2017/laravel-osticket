<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_staff extends Model
{
  protected $table = 'ost_staff';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
