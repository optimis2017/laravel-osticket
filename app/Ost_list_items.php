<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_list_items extends Model
{
  protected $table = 'ost_list_items';
  public $timestamps = false;
}
