<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sbbadmin extends Model
{
  protected $table = 'sbbadmin';
  public $timestamps = false;
}
