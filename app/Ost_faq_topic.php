<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_faq_topic extends Model
{
  protected $table = 'ost_faq_topic';
  public $timestamps = false;
}
