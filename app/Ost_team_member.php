<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_team_member extends Model
{
  protected $table = 'ost_team_member';
  const UPDATED_AT = 'updated';
}
