<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_group_dept_access extends Model
{
  protected $table = 'ost_group_dept_access';
  public $timestamps = false;
}
