<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CPMIClient extends Model
{
  protected $table = 'CPMIClient';
  public $timestamps = false;
}
