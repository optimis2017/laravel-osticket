<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_email_template_group extends Model
{
  protected $table = 'ost_email_template_group';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
