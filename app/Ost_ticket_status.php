<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_ticket_status extends Model
{
  protected $table = 'ost_ticket_status';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
