<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPTIMIS_configurateur extends Model
{
  protected $table = 'OPTIMIS_configurateur';
  public $timestamps = false;
}
