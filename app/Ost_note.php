<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_note extends Model
{
  protected $table = 'ost_note';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
