<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_groups extends Model
{
  protected $table = 'ost_groups';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
