<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CPMIPchemins extends Model
{
  protected $table = 'CPMIPchemins';
  public $timestamps = false;
}
