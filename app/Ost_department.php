<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_department extends Model
{
  protected $table = 'ost_department';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
