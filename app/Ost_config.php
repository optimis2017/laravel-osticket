<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_config extends Model
{
  protected $table = 'ost_config';
  const UPDATED_AT = 'updated';
}
