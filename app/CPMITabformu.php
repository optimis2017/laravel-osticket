<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CPMITabformu extends Model
{
  protected $table = 'CPMITabformu';
  public $timestamps = false;
}
