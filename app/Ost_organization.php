<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_organization extends Model
{
  protected $table = 'ost_organization';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
