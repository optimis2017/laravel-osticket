<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compteur_visite extends Model
{
  protected $table = 'compteur_visite';
  public $timestamps = false;
}
