<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_ticket_event extends Model
{
  protected $table = 'ost_ticket_event';
  const CREATED_AT = 'timestamp';
}
