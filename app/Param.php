<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Param extends Model
{
  protected $table = 'param';
  public $timestamps = false;
}
