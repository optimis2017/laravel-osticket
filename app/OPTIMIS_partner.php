<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPTIMIS_partner extends Model
{
  protected $table = 'OPTIMIS_partner';
  public $timestamps = false;
}
