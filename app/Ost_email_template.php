<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_email_template extends Model
{
  protected $table = 'ost_email_template';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
