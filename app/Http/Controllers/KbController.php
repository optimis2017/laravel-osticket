<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ost_faq;
use App\Ost_faq_topic;
use App\Ost_faq_category;
use App\Ost_ticket;
use App\Ost_ticket__cdata;
use App\Ost_stock_tickets;
use View;
class KbController extends Controller
{
   public function __construct() {
    $faq = Ost_faq::all();
    $category = Ost_faq_category::all();
    $topic = Ost_faq_topic::all();
    $ticket = Ost_ticket::all();
    $data = Ost_ticket__cdata::all();
    $stock = Ost_stock_tickets::all();
    View::share ( 'faq', $faq );
    View::share ( 'category', $category );
    View::share ( 'topic', $topic );
    View::share ( 'ticket', $ticket );
    View::share ( 'data', $data );
    View::share ( 'stock', $stock );
  }
}
