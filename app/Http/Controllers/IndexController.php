<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Session;
use App\http\controllers\KbController;
use App\Ost_organization;
class IndexController extends KbController
{
  public function __construct(){
       parent::__construct();
    }
    public function begin(){
      return view('begin');
    }
    public function view(){
      // echo "optimis";
      $org = Ost_organization::all();
      return view('index',['org' => $org]);
    }
    public function viewAvea(){
      // echo "avea";
      $org = Ost_organization::all();
      return view('index-avea',['org' => $org]);
    }
}
