<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_api_key extends Model
{
  protected $table = 'ost_api_key';
  public $timestamps = false;
}
