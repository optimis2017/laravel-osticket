<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_user extends Model
{
  protected $table = 'ost_user';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
