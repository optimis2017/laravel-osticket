<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_ticket_email_info extends Model
{
  protected $table = 'ost_ticket_email_info';
  public $timestamps = false;
}
