<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_filter_rule extends Model
{
  protected $table = 'ost_filter_rule';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
