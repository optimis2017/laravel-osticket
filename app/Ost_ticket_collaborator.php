<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_ticket_collaborator extends Model
{
  protected $table = 'ost_ticket_collaborator';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
