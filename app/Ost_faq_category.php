<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_faq_category extends Model
{
  protected $table = 'ost_faq_category';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
