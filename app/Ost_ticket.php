<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_ticket extends Model
{
  protected $table = 'ost_ticket';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
