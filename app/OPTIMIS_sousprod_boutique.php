<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPTIMIS_sousprod_boutique extends Model
{
  protected $table = 'OPTIMIS_sousprod_boutique';
  public $timestamps = false;
}
