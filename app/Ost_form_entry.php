<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_form_entry extends Model
{
  protected $table = 'ost_form_entry';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
