<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_faq extends Model
{
  protected $table = 'ost_faq';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
