<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPTIMIS_membre extends Model
{
  protected $table = 'OPTIMIS_membre';
  public $timestamps = false;
}
