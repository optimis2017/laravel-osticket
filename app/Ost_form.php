<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_form extends Model
{
  protected $table = 'ost_form';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
