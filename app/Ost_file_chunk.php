<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_file_chunk extends Model
{
  protected $table = 'ost_file_chunk';
  public $timestamps = false;
}
