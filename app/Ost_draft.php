<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_draft extends Model
{
  protected $table = 'ost_draft';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
