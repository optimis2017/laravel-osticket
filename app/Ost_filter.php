<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_filter extends Model
{
  protected $table = 'ost_filter';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
