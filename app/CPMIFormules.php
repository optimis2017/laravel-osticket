<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CPMIFormules extends Model
{
  protected $table = 'CPMIFormules';
  public $timestamps = false;
}
