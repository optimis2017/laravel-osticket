<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_help_topic extends Model
{
  protected $table = 'Ost_help_topic';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
