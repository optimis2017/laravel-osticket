<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPTIMIS_cat_boutique extends Model
{
  protected $table = 'OPTIMIS_cat_boutique';
  public $timestamps = false;
}
