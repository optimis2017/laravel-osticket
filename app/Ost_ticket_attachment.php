<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_ticket_attachment extends Model
{
  protected $table = 'ost_ticket_attachment';
  const CREATED_AT = 'created';
}
