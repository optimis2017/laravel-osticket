<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_plugin extends Model
{
  protected $table = 'ost_plugin';
  public $timestamps = false;
}
