<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPTIMIS_devis extends Model
{
  protected $table = 'OPTIMIS_devis';
  public $timestamps = false;
}
