<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_ticket_lock extends Model
{
  protected $table = 'ost_ticket_lock';
  const CREATED_AT = 'created';
}
