<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPTIMIS_presentation extends Model
{
  protected $table = 'OPTIMIS_presentation';
  public $timestamps = false;
}
