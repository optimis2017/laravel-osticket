<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_list extends Model
{
  protected $table = 'ost_list';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
