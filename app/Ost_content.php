<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_content extends Model
{
  protected $table = 'ost_content';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
