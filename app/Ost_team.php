<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_team extends Model
{
  protected $table = 'ost_team';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
