<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_ticket_thread extends Model
{
  protected $table = 'ost_ticket_thread';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
