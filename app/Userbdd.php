<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userbdd extends Model
{
  protected $table = 'user';
  public $timestamps = false;
}
