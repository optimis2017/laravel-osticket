<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_form_field extends Model
{
  protected $table = 'ost_form_field';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
