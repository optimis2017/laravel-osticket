<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPTIMIS_destination extends Model
{
  protected $table = 'OPTIMIS_destination';
  public $timestamps = false;
}
