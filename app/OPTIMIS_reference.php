<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPTIMIS_reference extends Model
{
  protected $table = 'OPTIMIS_reference';
  public $timestamps = false;
}
