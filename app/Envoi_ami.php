<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Envoi_ami extends Model
{
  protected $table = 'envoi_ami';
  public $timestamps = false;
}
