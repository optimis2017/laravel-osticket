<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_ticket_priority extends Model
{
  protected $table = 'ost_ticket_priority';
  public $timestamps = false;
}
