<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_sla extends Model
{
  protected $table = 'ost_sla';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
