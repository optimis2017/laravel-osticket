<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPTIMIS_souscat_boutique extends Model
{
  protected $table = 'OPTIMIS_souscat_boutique';
  public $timestamps = false;
}
