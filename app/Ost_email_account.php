<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_email_account extends Model
{
  protected $table = 'ost_email_account';
  public $timestamps = false;
}
