<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_timezone extends Model
{
  protected $table = 'ost_timezone';
  public $timestamps = false;
}
