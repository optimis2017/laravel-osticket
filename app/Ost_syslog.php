<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_syslog extends Model
{
  protected $table = 'ost_syslog';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
