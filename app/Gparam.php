<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gparam extends Model
{
  protected $table = 'gparam';
  public $timestamps = false;
}
