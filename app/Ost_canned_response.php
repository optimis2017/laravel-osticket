<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_canned_response extends Model
{
  protected $table = 'ost_canned_response';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
