<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_attachment extends Model
{
  protected $table = 'ost_attachment';
  public $timestamps = false;
}
