<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_email extends Model
{
  protected $table = 'ost_email';
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
}
