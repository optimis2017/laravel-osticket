<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_sequence extends Model
{
  protected $table = 'ost_sequence';
  const UPDATED_AT = 'updated';
}
