<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_form_entry_values extends Model
{
  protected $table = 'ost_form_entry_values';
  public $timestamps = false;
}
