<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_session extends Model
{
  protected $table = 'ost_session';
  const UPDATED_AT = 'session_updated';
}
