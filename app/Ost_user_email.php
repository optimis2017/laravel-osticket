<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_user_email extends Model
{
  protected $table = 'ost_user_email';
  public $timestamps = false;
}
