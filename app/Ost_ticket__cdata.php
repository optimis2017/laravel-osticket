<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ost_ticket__cdata extends Model
{
  protected $table = 'ost_ticket__cdata';
  public $timestamps = false;
}
